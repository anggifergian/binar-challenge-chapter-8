import Joi from "joi";
import React from "react";
import Form from "../common/form";

class PlayerCreate extends Form {
  state = {
    data: { username: "", email: "", experience: "", level: "" },
    errors: {},
  };

  schema = {
    username: Joi.string().min(5).required().label("Username"),
    email: Joi.string()
      .email({ tlds: { allow: false } })
      .required()
      .label("Email"),
    experience: Joi.string().required().label("Experience"),
    level: Joi.string().required().label("Level"),
  };

  doSubmit = () => {
    console.log("Submitted");
  };

  render() {
    return (
      <div>
        <h1 style={{ paddingBottom: 10 }}>Player Create</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("username", "Username")}
          {this.renderInput("email", "Email")}
          {this.renderInput("experience", "Experience")}
          {this.renderInput("level", "Level")}
          {this.renderButton("Save")}
        </form>
      </div>
    );
  }
}

export default PlayerCreate;
