import React, { Component } from "react";
import { Link } from "react-router-dom";
import { getPlayers } from "../../services/fakePlayers";
import Search from "../common/search";

class Player extends Component {
  state = {
    players: [],
    searchQuery: "",
  };

  componentDidMount() {
    const players = [...getPlayers()];
    this.setState({ players });
  }

  handleSearch = (query) => {
    this.setState({ searchQuery: query });
  };

  render() {
    const { players, searchQuery } = this.state;

    let filtered = players;

    if (searchQuery)
      filtered = players.filter((p) =>
        p.username.toLowerCase().startsWith(searchQuery.toLowerCase())
      );

    return (
      <>
        <Link to="/players/new">
          <button className="btn btn-primary">New Player</button>
        </Link>
        <div className="mt-3">
          <p>Showing {players.length} players in the database.</p>
          <Search onSearch={this.handleSearch} />
          <table className="table table-hover">
            <thead>
              <tr>
                <th>Username</th>
                <th>Email</th>
                <th>Experience</th>
                <th>Level</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {filtered.map((player) => {
                return (
                  <tr key={player.id}>
                    <td>{player.username}</td>
                    <td>{player.email}</td>
                    <td>{player.experience}</td>
                    <td>{player.level.label}</td>
                    <td>
                      <Link to={`/players/${player.id}`}>
                        <button className="btn btn-warning btn-sm">Edit</button>
                      </Link>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </>
    );
  }
}

export default Player;
