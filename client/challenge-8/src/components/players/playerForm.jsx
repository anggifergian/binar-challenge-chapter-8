import Form from "../common/form";
import Joi from "joi";
import { getLevel } from "../../services/fakeLevel";
import { getPlayer, savePlayer } from "../../services/fakePlayers";

class PlayerForm extends Form {
  state = {
    data: {
      username: "",
      email: "",
      experience: "",
      levelId: "",
    },
    levels: [],
    errors: {},
  };

  componentDidMount() {
    const levels = getLevel();
    this.setState({ levels });

    const playerId = this.props.match.params.id;
    if (playerId === "new") return;

    const player = getPlayer(playerId);
    if (!player) return this.props.history.replace("/not-found");

    this.setState({ data: this.mapToViewModel(player) });
  }

  mapToViewModel = (player) => {
    return {
      id: player.id,
      username: player.username,
      email: player.email,
      experience: player.experience,
      levelId: player.level._id,
    };
  };

  schema = {
    id: Joi.string(),
    username: Joi.string().min(5).required().label("Username"),
    email: Joi.string()
      .email({ tlds: { allow: false } })
      .required()
      .label("Email"),
    experience: Joi.string().required().label("Experience"),
    levelId: Joi.string().required().label("Level"),
  };

  doSubmit = () => {
    savePlayer(this.state.data);

    this.props.history.push("/players");
  };

  render() {
    return (
      <div>
        <h1 style={{ paddingBottom: 10 }}>Player Create</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("username", "Username")}
          {this.renderInput("email", "Email")}
          {this.renderInput("experience", "Experience")}
          {this.renderSelect("levelId", "Level", this.state.levels)}
          {this.renderButton("Save")}
        </form>
      </div>
    );
  }
}

export default PlayerForm;
