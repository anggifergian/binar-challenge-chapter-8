import React, { Component } from "react";
import { getPlayers } from "../../services/fakePlayers";

class PlayerEdit extends Component {
  state = {
    player: [],
  };

  componentDidMount() {
    const { match } = this.props;
    const playerSelect = getPlayers().find((p) => p.id === match.params.id);
    const player = [playerSelect];
    this.setState({ player });
  }

  render() {
    const { player } = this.state;
    return (
      <div>
        <h3>Edit Player</h3>
        <div>
          {player.map((p) => (
            <ul key={p.id}>
              <li>{p.username}</li>
              <li>{p.email}</li>
              <li>{p.experience}</li>
              <li>{p.level.label}</li>
            </ul>
          ))}
        </div>
      </div>
    );
  }
}

export default PlayerEdit;
