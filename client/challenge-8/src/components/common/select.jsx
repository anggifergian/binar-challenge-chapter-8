import React from "react";

const Select = ({ name, label, error, value, onChange, options }) => {
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <select
        className="form-control"
        id={name}
        name={name}
        style={error ? { borderColor: "red" } : null}
        onChange={onChange}
        value={value}
      >
        <option value=""></option>
        {options.map((option) => (
          <option value={option._id} key={option._id}>
            {option.label}
          </option>
        ))}
      </select>
      {error && (
        <div className="invalid-feedback" style={{ display: "block" }}>
          {error}
        </div>
      )}
    </div>
  );
};

export default Select;
