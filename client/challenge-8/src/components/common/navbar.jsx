import React from "react";
import Navbar from "react-bootstrap/Navbar";
import { NavLink, Link } from "react-router-dom";

const NavBar = () => {
  return (
    <Navbar bg="light">
      <div className="container">
        <Link to="/">
          <Navbar.Brand style={{ fontWeight: "bold" }}>Chapter 8</Navbar.Brand>
        </Link>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <NavLink className="nav-item nav-link" to="/players">
              Players
            </NavLink>
            <NavLink className="nav-item nav-link" to="/movies">
              Movies
            </NavLink>
            <NavLink className="nav-item nav-link" to="/login">
              Login
            </NavLink>
          </ul>
        </div>
      </div>
    </Navbar>
  );
};

export default NavBar;
