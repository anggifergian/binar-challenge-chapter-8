import { Component } from "react";
import Joi from "joi";
import Input from "./input";
import Select from "./select";

class Form extends Component {
  state = {
    data: {},
    errors: {},
  };

  validate = () => {
    const { data } = this.state;
    // Set schema
    const onSubmitSchema = Joi.object(this.schema);

    // Validate schema
    const options = { abortEarly: false };
    const { error } = onSubmitSchema.validate(data, options);

    if (!error) return null;

    // Storing array of error detail in an object
    const errors = {};
    error.details.map((m) => (errors[m.path[0]] = m.message));
    return errors;
  };

  handleSubmit = (e) => {
    // Prevent page is loading
    e.preventDefault();

    // Validate current data state
    const errors = this.validate();
    this.setState({ errors: errors || {} });
    if (errors) return;

    this.doSubmit();
  };

  validateProperty = ({ name, value }) => {
    // Set schema
    const onChangeSchema = Joi.object({ [name]: this.schema[name] });

    // Validate schema
    const { error } = onChangeSchema.validate({ [name]: value });
    return error ? error.details[0].message : null;
  };

  handleChange = ({ currentTarget: input }) => {
    // Validate input name and value
    const errorMessage = this.validateProperty(input);

    // Storing error message to errors variable
    const errors = { ...this.state.errors };
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];

    // Save input name and value to account state property
    const data = { ...this.state.data };
    data[input.name] = input.value;

    // Update local state
    this.setState({ data, errors });
  };

  renderButton(label) {
    return (
      <button disabled={this.validate()} className="btn btn-primary">
        {label}
      </button>
    );
  }

  renderInput(name, label, type = "text") {
    const { data, errors } = this.state;
    return (
      <Input
        type={type}
        value={data[name]}
        onChange={this.handleChange}
        name={name}
        label={label}
        error={errors[name]}
      />
    );
  }

  renderSelect(name, label, options) {
    const { data, errors } = this.state;
    return (
      <Select
        name={name}
        value={data[name]}
        onChange={this.handleChange}
        label={label}
        error={errors[name]}
        options={options}
      />
    );
  }
}

export default Form;
