export const levels = [
  { _id: "5b21ca3eeb7f6fbccd471818", label: "Beginner" },
  { _id: "5b21ca3eeb7f6fbccd471814", label: "Intermediate" },
  { _id: "5b21ca3eeb7f6fbccd471820", label: "Professional" },
];

export function getLevel() {
  return levels.filter((g) => g);
}
