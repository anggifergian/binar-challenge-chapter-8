import * as levelApi from "./fakeLevel";

const players = [
  {
    id: "1",
    username: "anggifergian",
    email: "anggifergian@gmail.com",
    experience: "fullstack developer",
    level: { _id: "5b21ca3eeb7f6fbccd471818", label: "Beginner" },
  },
  {
    id: "2",
    username: "andikadicky",
    email: "andikadicky@gmail.com",
    experience: "frontend developer",
    level: { _id: "5b21ca3eeb7f6fbccd471820", label: "Professional" },
  },
  {
    id: "3",
    username: "juliadi",
    email: "juliadi@gmail.com",
    experience: "backend developer",
    level: { _id: "5b21ca3eeb7f6fbccd471814", label: "Intermediate" },
  },
  {
    id: "4",
    username: "raihananshari",
    email: "raihananshari@gmail.com",
    experience: "system analyst",
    level: { _id: "5b21ca3eeb7f6fbccd471814", label: "Intermediate" },
  },
  {
    id: "5",
    username: "jatiutama",
    email: "jatiutama@gmail.com",
    experience: "ui/ux desiner",
    level: { _id: "5b21ca3eeb7f6fbccd471818", label: "Beginner" },
  },
];

export function getPlayers() {
  return players;
}

export function getPlayer(id) {
  return players.find((p) => p.id === id);
}

export function savePlayer(player) {
  let playerInDb = players.find((p) => p.id === player.id) || {};
  playerInDb.username = player.username;
  playerInDb.email = player.email;
  playerInDb.experience = player.experience;
  playerInDb.level = levelApi.levels.find((g) => g._id === player.levelId);

  if (!playerInDb.id) {
    playerInDb.id = (players.length + 1).toString();
    players.push(playerInDb);
  }

  return playerInDb;
}
