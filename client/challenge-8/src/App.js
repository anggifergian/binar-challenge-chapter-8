import React, { Component } from "react";
import Container from "react-bootstrap/Container";
import Player from "./components/players/players";
import NotFound from "./components/common/not-found";
import Movies from "./components/movies/movies";
import Navbar from "./components/common/navbar";
import { Route, Redirect, Switch } from "react-router-dom";
import PlayerForm from "./components/players/playerForm";
import Login from "./components/login";

class App extends Component {
  render() {
    return (
      <>
        <Navbar />
        <main>
          <Container className="mt-3">
            <Switch>
              <Route path="/players" exact>
                <Player />
              </Route>
              <Route path="/players/:id" component={PlayerForm}></Route>
              <Route path="/movies">
                <Movies />
              </Route>
              <Route path="/login">
                <Login />
              </Route>
              <Route path="/not-found">
                <NotFound />
              </Route>
              <Redirect from="/" exact to="/players" />
              <Redirect to="/not-found" />
            </Switch>
          </Container>
        </main>
      </>
    );
  }
}

export default App;
